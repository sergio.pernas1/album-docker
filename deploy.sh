#!/bin/bash

# El script debe recibir 3 argumentos.

if [ $# -ne 3 ];then

	echo "uso: ./deploy.sh opciones puerto nombre_contenedor"
	exit 1

fi



name="--name $3"

if [[ $2 =~ ^[0-9]+$ ]];then

	puerto="-p $2:80"
else
	echo "El puerto debe ser un numero entero menor a 65000."
	exit 1

fi

if [ $1 == "-c" ];then 

	docker build -t album-docker .

	docker run -d ${name} ${puerto} album-docker

elif [ $1 == "-d" ];then


	docker run -d ${name} ${puerto} album-docker
	
else
	echo "Usar opciones -d o -c"
	exit 1
fi





