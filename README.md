# Album Docker



**Clonar repositorio**

```
$ git clone https://gitlab.com/sergio.pernas1/album-docker.git
$ cd album-docker/
```

**Crear imagen**

```
$ sudo docker built -t album-docker .
```

**Desplegar contenedor**

```
$ docker run -d -p 80:80 album-docker
```

**Script de despliegue**

Crea/actualiza la imagen y despliega el contenedor..

```
./deploy.sh -c puerto deploy_name
```

Solo despliega contenedor si la imagen ya esta creada.

```
./deploy.sh puerto deploy_name
```
